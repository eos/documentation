Develop
=======

Source Code
-----------

For development clone the GIT source tree ...

``` {.sourceCode .bash}
git clone http://eos.cern.ch/repos/eos.git
```

Create a build directory ...

``` {.sourceCode .bash}
cd eos
mkdir build
cd build
```

Dependencies
------------

<div class="admonition warning">

Before compilation you have to make sure that you installed the packaged
listed in the following table ...

</div>

>   Package                  Version
>   ------------------------ -----------
>   xrootd-server            = 3.3.4
>   xrootd-server-devel      = 3.3.4
>   xrootd-private-devel     = 3.3.4
>   xrootd-cl-devel          = 3.3.4
>   readline-devel           default
>   readline                 default
>   ncurses-devel            default
>   ncurses-static default   
>   ncurses                  default
>   libattr-devel            default
>   libattr                  default
>   openldap-devel           default
>   openldap                 default
>   e2fsprogs-devel          default
>   e2fsprogs                default
>   zlib-devel               default
>   openssl-devel            default
>   ncurses-devel            default
>   xfsprogs-devel           default
>   xfsprogs                 default
>   fuse-devel               &gt;= 2.7
>   fuse-libs                &gt;= 2.7
>   fuse                     &gt;= 2.7
>   leveldb-devel            &gt;= 1.7
>   leveldb                  &gt;= 1.7
>   git                      default
>   cmake                    &gt;= 2.8
>   sparsehash-devel         default
>   libmicrohttpd            EOS rpm
>   libmicrohttpd-devel      EOS rpm
>   libuuid                  default
>   libuuid-devel            default
>   zeromq                   default
>   zeromq-devel             default
>   protobuf                 default
>   protobuf-devel           default
>   perl-Time-HiRes          default
>
There are two convenience scripts to install all dependencies in the EOS
source tree:

### SLC6

``` {.sourceCode .bash}
utils/sl6-packages.sh
```

### EL7

``` {.sourceCode .bash}
utils/el7-packages.sh
```

Compilation
-----------

Run *cmake* ...

``` {.sourceCode .bash}
cmake ../
```

Compile the project ...

``` {.sourceCode .bash}
make -j 4
make install
```
