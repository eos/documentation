EOS admin configuration
=======================

There are two types of nodes \* the MGM node (namespace node) \* the FST
nodes (storage nodes)

Copy example config file to /etc/sysconfig/eos

``` {.sourceCode .text}
cp /etc/sysconfig/eos.example /etc/sysconfig/eos
```

Setup MGM
---------

Change following variables in /etc/sysconfig/eos

Form MGM you should set following roles

``` {.sourceCode .text}
export XRD_ROLES="mq mgm fst"
```

Set eos name

<div class="admonition note">

EOS\_INSTANCE\_NAME has to start with "eos" and has form
"eos&lt;name&gt;". I will use &lt;name&gt; test for in this instruction,
but you can pick any name you like.

</div>

``` {.sourceCode .text}
export EOS_INSTANCE_NAME=eostest
```

change following variables

``` {.sourceCode .text}
export EOS_BROKER_URL=root://localhost:1097//eos/
export EOS_MGM_MASTER1=<MGM hostname>
export EOS_MGM_MASTER2=<MGM hostname>
export EOS_MGM_ALIAS=<MGM hostname>
export EOS_FUSE_MGM_ALIAS=<MGM hostname>
```

in /etc/xrd.cf.mgm change security setting as you need

``` {.sourceCode .text}
# Example disable krb5 and gsi
#sec.protocol krb5
#sec.protocol gsi
sec.protbind * only sss unix
```

If you are using systemd (el-7) you have to add these lines (e.g. at the
end) to the sysconfig file:

``` {.sourceCode .text}
which systemctl >& /dev/null
if [ $? -eq 0 ]; then
  alias service="service --skip-redirect"
fi
```

Let's start eos

``` {.sourceCode .text}
bash> service eos start
```

and let's enable sss security

``` {.sourceCode .text}
eos -b vid enable sss
```

and let's define sub-spaces

<div class="admonition note">

You should have atleast as many subspaces as the number of filesystems
per storage node.

</div>

if you have 20 disks on the storage nodes you do

``` {.sourceCode .text}
for name in `seq 1 20`; do eos -b group set default.$name on; done
```

Now you should see at lease one node (FST) via eos console (eos -b)

<div class="admonition note">

We see our MGM node because we added "fst" in XRD\_ROLES. You can remove
if you don't want to user your MGM machine as FST.

</div>

To create fuse mount on MGM you can do

``` {.sourceCode .text}
bash> mkdir /eos/
bash> service eosd start
```

Setup FST
---------

Let's create empty eos config file in /etc/sysconfig/eos and change
&lt;MGM hostname&gt;

``` {.sourceCode .text}
DAEMON_COREFILE_LIMIT=unlimited
export XRD_ROLES="fst"
export LD_PRELOAD=/usr/lib64/libjemalloc.so.1
export EOS_BROKER_URL=root://<MGM hostname>:1097//eos/
```

``` {.sourceCode .text}
bash> service eos start
```

Now on MGM node we should see our new FST

``` {.sourceCode .text}
eos -b node ls
```

you should see something similar

``` {.sourceCode .text}
[root@eos-head-iep-grid ~]# eos -b node ls
#-----------------------------------------------------------------------------------------------------------------------------
#     type #                       hostport #   status #     status # txgw #gw-queued # gw-ntx #gw-rate # heartbeatdelta #nofs
#-----------------------------------------------------------------------------------------------------------------------------
nodesview   eos-data-iep-grid.saske.sk:1095     online           on    off          0       10      120                1     1
nodesview   eos-head-iep-grid.saske.sk:1095     online           on    off          0       10      120                1     1
```

Now, let's add some file systems (some disk partitions)

<div class="admonition note">

Make sure that your data partition is having "user\_xattr" option on,
when you are mounting. Here is example in /etc/fstab

/dev/sdb1 /data01 ext4 defaults,user\_xattr 0 0

</div>

Let's assume that you have 4 partitions /data01 /data02 /data03 /data04.
You have to change owner to daemon:daemon

``` {.sourceCode .text}
chown -R daemon:daemon /data*
```

and register them on MGM via

``` {.sourceCode .text}
eosfstregister /data default:4
```

<div class="admonition note">

!!! i have no idea how to unregister !!!!

</div>

Finish MGM
----------

To enable all hosts and filesystems in default space do following:

``` {.sourceCode .text}
eos -b space set default on
```

To see our space you do

``` {.sourceCode .text}
eos -b space ls
```

And you should see space in "RW" and then you are ready to go.

Testing MGM
-----------

Preapre for testing let's do following changes

``` {.sourceCode .text}
# disable quota for first tests
eos -b space quota default off

# disable balancer
eos -b space config default space.balancer=off

# set balancer threshold to 5%
eos -b space config default space.balancer.threshold=5

# set checksum scan interval to 1 week
eos -b space config default space.scaninterval=604800

# set drain delay for IO errors to 1 hours
eos -b space config default space.graceperiod=3600

# set max drain time to 1 day
eos -b space config default space.drainperiod=86400
```

Enable kerberos security
------------------------
Enable kerberos security
========================

First you need setup user maping (via /etc/passwd, ldap, ...) on MGM and
your gol is to see for example user info

<div class="admonition note">

In this case i have mvala user from alice group.

</div>

``` {.sourceCode .text}
[root@eos-head-iep-grid ~]# id mvala
uid=10000(mvala) gid=10000(alice) groups=10000(alice)
```

Please install krb5 packages

``` {.sourceCode .text}
yum install krb5-workstation
```

Then you need to ask kerberos admin to create "host/&lt;mgm
hostname&gt;@EXAMPLE.COM", where EXAMPE.COM is your REALM (like CERN.CH,
SASKE.SK, ...) and create keytab file, for example krb5.keytab. Then you
need to save this file at /etc/krb5.keytab on MGM node. To test it you
can use ktutil command. Following example is showing keytab needs to be
used on MGM host eos-head-iep-grid.saske.sk

``` {.sourceCode .text}
[root@eos-head-iep-grid ~]# ktutil 
ktutil:
ktutil:  read_kt /etc/krb5.keytab
ktutil:  list
slot KVNO Principal
---- ---- ---------------------------------------------------------------------
   1    2 host/eos-head-iep-grid.saske.sk@SASKE.SK
   2    2 host/eos-head-iep-grid.saske.sk@SASKE.SK
   3    2 host/eos-head-iep-grid.saske.sk@SASKE.SK
   4    2 host/eos-head-iep-grid.saske.sk@SASKE.SK
```

Then on MGM in /etc/xrd.cf.mgm you shoudld have following line

``` {.sourceCode .text}
sec.protocol krb5 -exptkn:/var/eos/auth/krb5#<uid> host/<host>@EXAMPLE.COM

sec.protbind * only krb5 sss unix
```

To enable krb5 security do

``` {.sourceCode .text}
eos -b vid enable krb5
```

Setup AUTH plugin
-----------------

The authentication plugin is inteded to be used as an OFS library with a
vanilla XRootD server. What it does is to connect using ZMQ sockets to
the real MGM nodes (in general it should connect to a master and a slave
MGM). It does this by reading out the endpoints it needs to connect to
from the configuration file (/etc/xrd.cf.auth). These need to follow the
format: "host:port" and the first one should be the endpoint
corresponding to the master MGM and the second one to the slave MGM. The
EosAuthOfs plugin then tries to replay all the requests it receives from
the clients to the master MGM node. It does this by marshalling the
request and identity of the client using ProtocolBuffers and sends this
request using ZMQ to the master MGM node. The authentication plugin can
run on the same machine as an MGM node or on a different machine. Once
can use several such authentication services at the same time.

There are several tunable parameters for this configuration (auth +
MGMs):

AUTH - configuration
--------------------

-   

    **eosauth.mastermgm** and **eosauth.slavemgm** - contain the hostnames and the

    :   ports to which ZMQ can connect to the MGM nodes so that it can
        forward requests and receive responses. Only the mastermgm
        parameter is mandatory the other one is optional and can be left
        out.

-   

    **eosauth.numsockets** - once a clients wants to send a request the thread

    :   allocated to him in XRootD will require a socket to send the
        request to the MGM node. Therefore, we set up a pool of sockets
        from the begining which can be used to send/receiver
        requests/responses. The default size is 10 sockets.

MGM - configuration
-------------------

-   

    **mgmofs.auththreads** - since we now receive requests using ZMQ, we no longer

    :   use the default thread pool from XRootD and we need threads for
        dealing with the requests. This parameter sets the thread pool
        size when starting the MGM node.

-   

    **mgmofs.authport** - this is the endpoint where the MGM listens for ZMQ

    :   requests from any EosAuthOfs plugins. This port needs to be
        opened also in the firewall.

In case of a master &lt;=&gt; slave switch the EosAuthOfs plugin adapts
automatically based on the information provided by the slave MGM which
should redirect all clients with write requests to the master node. Care
should be taken when specifying the two endpoints since the switch is
done ONLY IF the redirection HOST matches one of the two endpoints
specified in the configuration of the authentication plugin (namely
eosauth.instance). Once the switch is done all requests be them read or
write are sent to the new master MGM node.
