Installation of EOS
===================

<div class="admonition note">

You need to add the EOS repositories. Please follow instruction from
eos\_base\_setup\_repos

</div>

Install eos via yum
-------------------

For client

``` {.sourceCode .text}
yum install eos-client
```

For server

``` {.sourceCode .text}
yum install eos-server eos-client eos-testkeytab eos-fuse jemalloc
```

For HTTP server

``` {.sourceCode .text}
yum install eos-nginx
```

For GridFTP server

> yum install xrootd-dsi

For EOS test on MGM (namespace node)

``` {.sourceCode .text}
yum install eos-test
```
