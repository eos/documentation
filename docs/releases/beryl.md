orphan

:   

Beryl
=====

`Lifetime: 2013-2015`

The **Beryl** release is the second generation of the production version
of EOS. It has been used since September 2013.

Here is a list of new features coming with this version

>   Feature                          Description
>   -------------------------------- -----------------------------------------------------------------
>   ../configuration/autorepair      Autorepair broken files detected during an FST scan
>   ../configuration/balancing       Re-implementation of balancing algorithm
>   ../configuration/converter       Asynchronous conversion queue doing file conversion
>   ../configuration/draining        Re-implementation of drain algorithm
>   ../configuration/fuse            Improved FUSE client
>   ../configuration/geobalancer     Geo-aware rebalancing
>   ../configuration/groupbalancer   Inter-Group balancing for storage expansion
>   ../configuration/http            HTTP(S)/WebDav interface
>   ../configuration/lru             LRU based policy engine
>   ../configuration/master          Master-Slave MGM (namespace)
>   ../configuration/permission      Extension of the permission system (modification,update rights)
>   ../configuration/quota           Extension of the quota system (project quota)
>   ../configuration/recyclebin      Recycle bin to undo deletions
>   ../using/rain                    Erasure-encoded files
>   ../using/tpc                     Third-party copy support
>
Release notes ./beryl-release
