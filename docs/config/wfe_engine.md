WFE Engine
==========

The workflow engine is a versatile event triggered storage process
chain. Currently all events are created by file operations. The policy
to emit events is described as extended attributes of a parent
directory. Each workflow is named. The default workflow is named
'default' and used if no workflow name is provided in an URL as
?eos.workflow=default. The workflow engine allows to create chained
workflows e.g. one workflow can trigger an event emission to run the
next workflow in the chain aso.

>   Event           Description
>   --------------- -------------------------------------------------------------------------------------
>   open            event is triggered at the MGM when a 'file open'
>                   - if the return of an open call is ENONET a workflow defined stall time is returned
>   sync::prepare   event is triggered at the MGM when a 'prepare' is issued (synchronous event)
>   closer          event is triggered via the MGM when a read-open file is closed on an FST.
>   closew          event is triggered via the MGM when a write-open file is closed on an FST
>   sync::delete    event is triggered at the MGM when a file has been deleted (synchronous event)
>
Currently the workflow engine implements two action targets. The
**bash:shell** target is a powerful target. It allows you to execute any
shell command as a workflow. This target provides a large set of
template parameters which EOS can give as input arguments to the called
shell command. This is described later. The **mail** target allows to
send an email notification to a specified recipient and mostly used for
demonstration.

>   Action Target   Description
>   --------------- ------------------------------------------------------------------------------------
>   bash:shell      run an arbitrary shell command line with template argument substitution
>   mail            send an email notification to a provided recipient when such an event is triggered
>
Configuration
-------------

### Engine

The WFE engine has to be enabled/disabled in the default space only:

``` {.sourceCode .bash}
# enable
eos space config default space.wfe=on  
# disable
eos space config default space.wfe=off
```

The current status of the WFE can be seen via:

``` {.sourceCode .bash}
eos -b space status default
# ------------------------------------------------------------------------------------
# Space Variables
# ....................................................................................
...
wfe                            := off
wfe.interval                   := 10
...
```

The interval in which the WFE engine is running is defined by the
**wfe.interval** space variable. The default is 10 seconds if
unspecified.

``` {.sourceCode .bash}
# run the LRU scan once a week
eos space config default space.wfe.interval=10
```

The thread-pool size of concurrently running workflows is defined by the
**wfe.ntx** space variable. The default is to run all workflow jobs
sequentially with a single thread.

``` {.sourceCode .bash}
# configure a thread pool of 16 workflow jobs in parallel
eos space config default space.wfe.ntx=10
```

Workflows are stored in a virtual queue system. The queues display the
status of each workflow. By default workflows older than 7 days are
cleaned up. This setting can be changed by the **wfe.keeptime** space
variable. That is the time in seconds how long workflows are kept in the
virtual queue system before they get deleted.

``` {.sourceCode .bash}
# keep workflows for 1 week
eos space config default space.wfe.keeptime=604800
```

### Workflow Configuration

#### The **mail** workflow

As an example we want to send an email to a mailing list, whenever a
file is deposited. This workflow can be specified like this:

``` {.sourceCode .bash}
# define a workflow to send when a file is written
eos attr set sys.workflow.closew.default="mail:eos-project.cern.ch: a file has been written!" /eos/dev/mail/

# place a new file
eos cp /etc/passwd /eos/dev/mail/passwd

# eos-project.cern.ch will receive an Email with a subject like: eosdev ( eosdev1.cern.ch ) event=closew fxid=000004f7 )
# and the text in the body : a file has been written!
```

#### The **bash:shell** workflow

Most people want to run a command whenever a file is placed, read or
deleted. To invoke a shell command one configures the **bash:shell**
workflow. As an example consider this simple echo command, which prints
the path when a **closew** event is triggered:

``` {.sourceCode .bash
# define a workflow to echo the full path when a file is written
eos attr set "sys.workflow.closew.default=sys.workflow.closew.default="bash:shell:mylog echo <eos::wfe::path>" /eos/dev/echo/}
```

The template parameters `<eos::wfe::path>` is replaced with the full
logical path of the file, which was written. The third parameters
`mylog` in **bash:shell:mylog** specifies the name of the log file for
this workflow which is found on the MGM under
`/var/log/eos/wfe/mylog.log`

Once one uploads a file into the `echo` directory, the following log
entry is created in `/var/log/eos/wfe/mylog.log`

``` {.sourceCode .bash}
----------------------------------------------------------------------------------------------------------------------
1466173303 Fri Jun 17 16:21:43 CEST 2016 shell echo /eos/dev/echo/passwd
/eos/dev/echo/passwd
retc=0
```

The full list of static template arguments is given here:

>   Template                            Description
>   ----------------------------------- ---------------------------------------------------------------------------------------------------------------
>   &lt;eos::wfe::uid&gt;               user id of the file owner
>   &lt;eos::wfe::gid&gt;               group id of the file owner
>   &lt;eos::wfe::username&gt;          user name of the file owner
>   &lt;eos::wfe::groupname&gt;         group name of the file owner
>   &lt;eos::wfe::ruid&gt;              user id invoking the workflow
>   &lt;eos::wfe::rgid&gt;              group id invoking the workflow
>   &lt;eos::wfe::rusername&gt;         user name invoking the workflow
>   &lt;eos::wfe::rgroupname&gt;        group name invoking the workflow
>   &lt;eos::wfe::path&gt;              full absolute file path which has triggered the workflow
>   &lt;eos::wfe::base64:path&gt;       base64 encoded full absolute file path which has triggered the workflow
>   &lt;eos::wfe::turl&gt;              XRootD transfer URL providing access by file id e.g. root://myeos.cern.ch//mydir/myfile?eos.lfn=fxid:00001aaa
>   &lt;eos::wfe::host&gt;              client host name triggering the workflow
>   &lt;eos::wfe::sec.app&gt;           client application triggering the workflow (this is defined externally via the CGI `?eos.app=myapp`)
>   &lt;eos::wfe::sec.name&gt;          client security credential name triggering the workflow
>   &lt;eos::wfe::sec.prot&gt;          client security protocol triggering the workflow
>   &lt;eos::wfe::sec.grps&gt;          client security groups triggering the workflow
>   &lt;eos::wfe::instance&gt;          EOS instance name
>   &lt;eos::wfe::ctime.s&gt;           file creation time seconds
>   &lt;eos::wfe::ctime.ns&gt;          file creation time nanoseconds
>   &lt;eos::wfe::mtime.s&gt;           file modification time seconds
>   &lt;eos::wfe::mtime.ns&gt;          file modification time nanoseconds
>   &lt;eos::wfe::size&gt;              file size
>   &lt;eos::wfe::cid&gt;               parent container id
>   &lt;eos::wfe::fid&gt;               file id (decimal)
>   &lt;eos::wfe::fxid&gt;              file id (hexacdecimal)
>   &lt;eos::wfe::name&gt;              basename of the file
>   &lt;eos::wfe::base64:name&gt;       base64 encoded basename of the file
>   &lt;eos::wfe::link&gt;              resolved symlink path if the original file path is a symbolic link to a file
>   &lt;eos::wfe::base64:link&gt;       base64 encoded resolved symlink path if the original file path is a symbolic link to a file
>   &lt;eos::wfe::checksum&gt;          checksum string
>   &lt;eos::wfe::checksumtype&gt;      checksum type string
>   &lt;eos::wfe::event&gt;             event name triggering this workflow (e.g. closew)
>   &lt;eos::wfe::queue&gt;             queue name triggering this workflow (e.g. can be 'q' or 'e')
>   &lt;eos::wfe::workflow&gt;          workflow name triggering this workflow (e.g. default)
>   &lt;eos::wfe::now&gt;               current unix timestamp when running this workflow
>   &lt;eos::wfe::when&gt;              scheduling unix timestamp when to run this workflow
>   &lt;eos::wfe::base64:metadata&gt;   a full base64 encoded meta data blop with all file metadata and parent metadata including extended attributes
>   &lt;eos::wfe::vpath&gt;             the path of the workflow file in the virtual workflow directory when the workflow is executed
>                                       - you can use this to attach messages/log as an extended attribute to a workflow if desired
>
Extended attributes of a file and it's parent container can be read with
dynamic template arguments:

>   Template                                      Description
>   --------------------------------------------- -----------------------------------------------------------------------------------------------------------
>   &lt;eos::wfe::fxattr:&lt;key&gt;&gt;          Retrieves the value of the extended attribute of the triggering file with name &lt;key&gt;
>                                                 - sets UNDEF if not existing
>   &lt;eos::wfe::fxattr:base64:&lt;key&gt;&gt;   Retrieves the base64 encoded value of the extended attribute of the triggering file with name &lt;key&gt;
>                                                 - sets UNDEF if not existing
>   &lt;eos::wfe::cxattr:&lt;key&gt;&gt;          Retrieves the value of the extended attribute of parent directory of the triggering file
>                                                 - sets UNDEF if not existing
>
Here is an example for a dynamic attribute:

``` {.sourceCode .bash}
# define a workflow to echo the meta blob and the acls of the parent directory when a file is written
eos attr set "sys.workflow.closew.default=sys.workflow.closew.default="bash:shell:mylog echo <eos::wfe::base64:metadata> <eos::wfe::cxattr:sys.acl>" /eos/dev/echo/
```

Configuring retry policies for **bash:shell** workflows
```````````````````````````` `  If a **bash:shell** workflow failes e.g. the command returns rc!=0 and no retry policy is defined, the workflow job ends up in the **failed** queue. For each  workflow the number of retries and the delay for retry can be defined via extended attributes. To reschedule a workflow after a failure the shell command has to return **EAGAIN** e.g. ``exit(11)`. The number of retries for a failing workflow can be defined as:  .. code-block:: bash     # define a workflow to return EAGAIN to be retried    eos attr set "sys.workflow.closew.default=sys.workflow.closew.default="bash:shell:fail '(exit 11)'" /eos/dev/echo/     # set the maximum number of retries    eos attr set "sys.workflow.closew.default.retry.max=3" /eos/dev/echo/  The previous workflow will be scheduled three times without delay. If you want to schedule a retry at a later point in time, you can define the delay for retry for a particular workflow like:  .. code-block:: bash     # configure a workflow retry after 1 hour    eos attr set "sys.workflow.closew.default.retry.delay=3600" /eos/dev/echo/   Returning result attributes``````````````if a **bash::shell** workflow is used, the STDERR of the command is parsed for return attribute tags, which are either tagged on the triggering file (path) or the virtual workflow entry (vpath):  .. epigraph::     ============================================== =====================================================================================    Syntax                                         Resulting Action    ============================================== =====================================================================================    <eos::wfe::path::fxattr:<key>>=base64:<value>  set a file attribute <key> on <eos::wfe::path> to the base64 decoded value of <value>    <eos::wfe::path::fxattr:<key>>=<value>         set a file attribute <key> on <eos::wfe::path> to <value> (value can not contain space)    <eos::wfe::vpath::fxattr:<key>>=base64:<value> set a file attribute <key> on <eos::wfe::vpath> to the base64 decoded value of <value>    <eos::wfe::vpath::fxattr:<key>>=:<value>       set a file attribute <key> on <eos::wfe::vpath> to <value> (value can not contain space)    ============================================== =====================================================================================  Virtual /proc Workflow queue directories ++++++++++++++++++++++++++++++++++++++++++++  The virtual directory structure for triggered workflows can be found under`/eos/&lt;instance&gt;/proc/workflow`.   Here is an example:  .. code-block:: bash     EOS Console [root://localhost] |/eos/dev/> eos find /eos/dev/proc/workflow/    /eos/dev/proc/workflow/20160617/d/    /eos/dev/proc/workflow/20160617/d/default/    /eos/dev/proc/workflow/20160617/d/default/1466171933:000004f7:closew    /eos/dev/proc/workflow/20160617/d/default/1466173303:000004fd:closew    /eos/dev/proc/workflow/20160617/f/    /eos/dev/proc/workflow/20160617/f/default/    /eos/dev/proc/workflow/20160617/f/default/1466171873:000004f4:closew    /eos/dev/proc/workflow/20160617/f/default/1466173183:000004fa:closew    /eos/dev/proc/workflow/20160617/q/    /eos/dev/proc/workflow/20160617/q/default/1466173283:000004fb:closew  The virtual tree is organized with entries like`&lt;proc&gt;/workflow/&lt;year-month-day&gt;/&lt;queue&gt;/&lt;workflow&gt;/&lt;unix-timestamp&gt;:&lt;fid&gt;:&lt;event&gt;`. Workflows are scheduled only from the **q** and **e** queues. All other entries describe a`finale
state`and will be expired as configured by the cleanup policy described in the beginning.  The existing queues are described here:  .. epigraph::     =========================== ========================================================================================    Queue                       Description    =========================== ========================================================================================    ../q/..                     all triggered asynchronous workflows appear first in this queue    ../s/..                     scheduled asynchronous workflows and triggered synchronous workflows appear in this queue    ../r/..                     running workflows appear in this queue    ../e/..                     failed workflows with retry policy appear here    ../f/..                     failed workflows without retry appear here    ../g/..                     workflows with 'gone' files or some global misconfiguration appear here    ../d/..                     successful workflows with 0 return code    =========================== ========================================================================================   Synchronous workflows```````````

The **deletion** and **prepare** workflow are synchronous workflows
which are executed in-line. They are stored and tracked as asynchronous
workflows in the proc filesystem. The emitted event on deletion is
**sync::delete**, the emitted event on prepare is **sync::prepare**.

Workflow log and return codes
-----------------------------

The return codes and log information is tagged on the virtual directory
entries in the proc filesystem as extended attributes:

``` {.sourceCode .bash}
sys.wfe.retc=<return code value>
sys.wfe.log=<message describing the result of running the workflow>
```
