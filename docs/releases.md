Releases
========

>   Release            Stable Version       Description          Release Notes
>   ------------------ -------------------- -------------------- --------------------------
>   releases/amber     0.2.47               1st EOS Generation   
>   releases/beryl     0.3.258-aquamarine   2nd EOS Generation   releases/beryl-release
>   releases/citrine   4.1.25               3nd EOS Generation   releases/citrine-release
>   releases/diamond   Future Development   4th EOS Generation   
>

