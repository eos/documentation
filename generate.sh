#!/bin/bash

AREA=${AREA:-"/eos/project/e/eos/www/docs"}

set -x
./node_modules/gitbook-cli/bin/gitbook.js build --log=info --format=website "${PWD}" "${AREA}"


cat >${AREA}/.htaccess <<EOF
ShibRequireAll Off
ShibRequireSession Off
ShibExportAssertion Off

Satisfy Any

Allow from all
EOF

